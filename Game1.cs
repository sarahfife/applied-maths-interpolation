﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace applied_maths_interpolation
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int displayWidth = 1080;
        int displayHeight = 720;

        Texture2D image;
        Vector2 position;


        // Interpolation data
        Vector2 begin;
        Vector2 change;
        float duration;
        float time;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = displayWidth;
            graphics.PreferredBackBufferHeight = displayHeight;
            graphics.ApplyChanges();

            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load in ball sprite for use
            image = Content.Load<Texture2D>("Ball");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // Check the mouse state
            MouseState mouse = Mouse.GetState();
            if (mouse.LeftButton == ButtonState.Pressed)
            {
                // If the player has clicked,
                // Start the transition

                // Set all of our interpolation data

                // Begin is the starting position for the transition
                begin = position;

                // End is our target position to end at (moust click position)
                Vector2 end = new Vector2(mouse.Position.X, mouse.Position.Y);

                // THe direction and distance we will move by the end of the transition
                change = end - begin;

                // How long the transition will take in seconds
                //duration = 1f;
                // Optional: calculate the duration based on an average desired speed
                float speed = 300f; // pixels / second
                duration = change.Length() / speed;

                // How long it has been since the start of the transition
                time = 0;
            }

            // Add to the time since we start our transition
            time += (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Check if our transition is over
            if (time >= duration)
            {
                // the transition has ended
                // Set us to be at our target position
                position = begin + change;
            }
            else
            {
                // transition is still going

                // Use the derived equation to perform interpolation
                // Instant change:
                //position = begin + change;

                // Linear interpolation:
                Vector2 k1 = change / duration;
                Vector2 k2 = begin;
                position = k1 * time + k2;

                // Quad Ease In
                //Vector2 k1 = change / (duration * duration);
                //Vector2 k2 = Vector2.Zero;
                //Vector2 k3 = begin;
                //position = k1 * time * time + k2 * time + k3;
            }



            base.Update(gameTime);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            spriteBatch.Draw(image, position, Color.White);

            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
